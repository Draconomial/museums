# Features
- Fence-like buildings to restrict access to museum visitors
- Signs with different icons
- Display-buildings and pedestals that can display resources
- Room role: Museum
    - Requires at least one watchable building and a museum gathering spot
- Joy logic for solo-touring a museum
- Gatherings for colonists to go on a guided tour
    - Work like parties, triggered randomly with a random organizer
- Watchable Buildings
    - Supported buildings have new gizmos, with which you can define positions from which pawns may look at that building whilst on a museum tour


# Mod Support:
## Makes buildings watchable:
- Weapon racks from [Aelannas Weapon Racks](https://steamcommunity.com/sharedfiles/filedetails/?id=2788630748)
- Paintings from [Roo's Painting Expansion](https://steamcommunity.com/sharedfiles/filedetails/?id=2881104358)
- Display cases from [Display Cases (Continued)](https://steamcommunity.com/sharedfiles/filedetails/?id=2086054868)
## Deeper integrations:
### [El's Archeology](https://steamcommunity.com/sharedfiles/filedetails/?id=3132100846)
- Removes built-in museum logic
- Make displays watchable
### [Hospitality](https://steamcommunity.com/sharedfiles/filedetails/?id=753498552)
- Allow special guided guest tour through Museum Gathering Spot
### [Biomes! Fossils](https://steamcommunity.com/sharedfiles/filedetails/?id=3100958580)
- Removes built-in museum logic
- Make various museum-related buildings watchable

Feel like there is a mod missing in this list? Tell me and I'll try to integrate it!

GitLab link: https://gitlab.com/nightcorp/museums/-/archive/master/museums-master.zip