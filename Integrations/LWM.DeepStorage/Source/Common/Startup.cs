﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;

namespace MuseumsIntegration_DeepStorage
{
    [StaticConstructorOnStartup]
    public static class Startup
    {
        static Startup()
        {
            Harmony harmony = new Harmony("MuseumsIntegration_DeepStorage");
            harmony.PatchAll();
        }
    }
}
