﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace MuseumsIntegration_Hospitality
{
    public class MapComponent_GuestGuidedTourTracker : MapComponent
    {
        const int guidedGuestTourCooldown = GenDate.TicksPerDay;
        int lastTourTick = -1;

        public MapComponent_GuestGuidedTourTracker(Map map) : base(map) { }

        public int TicksUntilNextTourAvailable
        {
            get
            {
                if(lastTourTick == -1)
                {
                    return 0;
                }
                return Mathf.Max(0, guidedGuestTourCooldown - (GenTicks.TicksGame - lastTourTick));
            }
        }

        public bool CanStartGuidedGuestTour => TicksUntilNextTourAvailable == 0;

        public void Notify_GuestTourStarted()
        {
            lastTourTick = GenTicks.TicksGame;
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Values.Look(ref lastTourTick, nameof(lastTourTick));
        }
    }
}
