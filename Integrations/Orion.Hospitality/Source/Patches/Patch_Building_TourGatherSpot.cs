﻿using HarmonyLib;
using Hospitality;
using Museums;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.AI.Group;
using Verse.Noise;

namespace MuseumsIntegration_Hospitality
{
    [HarmonyPatch(typeof(Building_TourGatherSpot), nameof(Building_TourGatherSpot.GetGizmos))]
    public class Patch_Building_TourGatherSpot
    {
        [HarmonyPostfix]
        public static IEnumerable<Gizmo> AddGuestTourGizmo(IEnumerable<Gizmo> __result, Building_TourGatherSpot __instance)
        {
            foreach (Gizmo gizmo in __result)
            {
                yield return gizmo;
            }
            yield return MakeStartGuestTourGizmo(__instance);
        }

        static Gizmo MakeStartGuestTourGizmo(Building_TourGatherSpot gatherSpot)
        {
            AcceptanceReport canStartReport = CanStartTour(gatherSpot);
            Command_Action gizmo = new Command_Action()
            {
                defaultLabel = "Museums_Hospitality_StartGuestTour".Translate(),
                defaultDesc = "Museums_Hospitality_StartGuestTour_Desc".Translate(),
                icon = TexCommand.GatherSpotActive,
                
            };
            if (canStartReport)
            {
                gizmo.action = () => StartTour(gatherSpot);
            }
            else
            {
#if v1_4
                gizmo.disabled = true;
#else
                gizmo.Disabled = true;
#endif
                gizmo.disabledReason = canStartReport.Reason;
            }
            return gizmo;
        }

        private static AcceptanceReport CanStartTour(Building_TourGatherSpot gatherSpot)
        {
            Map map = gatherSpot.Map;
            Hospitality_MapComponent comp = map.GetComponent<Hospitality_MapComponent>();
            if (comp.PresentGuests.EnumerableNullOrEmpty())
            {
                return "Museums_Hospitality_StartGuestTour_Invalid_NoGuests".Translate();
            }

            MapComponent_GuestGuidedTourTracker guestTourComp = map.GetComponent<MapComponent_GuestGuidedTourTracker>();
            if (!guestTourComp.CanStartGuidedGuestTour)
            {
                NamedArgument cooldownFormatted = guestTourComp.TicksUntilNextTourAvailable.ToStringTicksToPeriodVerbose().Named("COOLDOWN");
                return "Museums_Hospitality_StartGuestTour_Invalid_Cooldown".Translate(cooldownFormatted);
            }

            MapComponent_MuseumTracker museumComp = map.GetComponent<MapComponent_MuseumTracker>();
            MuseumData museumData = museumComp.GetMuseumData(gatherSpot.GetRoom());
            AcceptanceReport tourReport = museumData.CanBeToured;
            if (!tourReport)
            {
                return tourReport;
            }

            return true;
        }

        static GatheringDef guidedTourGatheringDef = DefDatabase<GatheringDef>.GetNamed("Museums_GuidedTour");
        private static void StartTour(Building_TourGatherSpot gatherSpot)
        {
            Map map = gatherSpot.Map;
            Pawn guide = GetOrganizer(map);
            LordJob_Joinable_GuidedMuseumTour guidedTourLordJob = new LordJob_Joinable_GuidedMuseumTour(guide, gatherSpot.Position, Common.GuidedTourGatheringDef);

            // only add guide for now, that way the pawn is certain to be the organoizer
            Lord newLord = LordMaker.MakeNewLord(Faction.OfPlayer, guidedTourLordJob, map, new Pawn[1] { guide });

            Hospitality_MapComponent comp = map.GetComponent<Hospitality_MapComponent>();
            // unassign guests from their lord job (they will be re-assigned to their lord after the tour ends)
            foreach(Pawn guestPawn in comp.PresentGuests)
            {
                Lord guestLord = guestPawn.GetLord();
                if(guestLord != null)
                {
                    guidedTourLordJob.RememberReassignedPawn(guestPawn);
                    guestLord.RemovePawn(guestPawn);
                }
                newLord.AddPawn(guestPawn);
                // visitors *love* standing around socializing for very long before they get the idea to check their jobs, so interrupt the lollygagging
                guestPawn.jobs.EndCurrentJob(Verse.AI.JobCondition.InterruptForced);
            }

            map.GetComponent<MapComponent_GuestGuidedTourTracker>().Notify_GuestTourStarted();
        }

        private static Pawn GetOrganizer(Map map)
        {
            return map.mapPawns.FreeColonists.RandomElement();
        }
    }
}
