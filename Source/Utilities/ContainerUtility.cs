﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;

namespace Museums
{
    /// <summary>
    /// Intended to be patched and extended by uxiliary mod integrations if those mods implement non-CompThingContainer comps (like LWM_DeepStorage)
    /// </summary>
    public class ContainerUtility
    {
        /// <summary>
        /// Mods may add their own implementation of base games CompThingContainer, to prevent hard dependencies and lazy loading, this method will be patched by integrations for those mods.
        /// </summary>
        public static bool ContainsItem(ThingWithComps thing)
        {
            // no idea if it would be possible/smart for a building to implement multiple ways of holding items, but hey, future proofing.

            if (thing is IThingHolder thingHolder)
            {
                if (thingHolder.GetDirectlyHeldThings().Any())
                {
                    return true;
                }
            }

            // covers Building_Storage
            if(thing is ISlotGroupParent slotGroupParent)
            {
                if (slotGroupParent.GetSlotGroup().HeldThings.Any())
                {
                    return true;
                }
            }

            CompThingContainer containerComp = thing.GetComp<CompThingContainer>();
            if (containerComp != null)
            {
                return !containerComp.Empty;
            }

            return false;
        }

        public static bool IsContainer(ThingDef thingDef)
        {
            if (typeof(IThingHolder).IsAssignableFrom(thingDef.thingClass))
            {
                return true;
            }
            // covers Building_Storage
            if (typeof(ISlotGroupParent).IsAssignableFrom(thingDef.thingClass))
            {
                return true;
            }
            if (thingDef.HasAssignableCompFrom(typeof(CompThingContainer)))
            {
                return true;
            }
            return false;
        }
    }
}
