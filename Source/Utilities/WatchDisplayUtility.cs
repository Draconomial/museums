﻿using HarmonyLib;
using Mono.Unix.Native;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.AI;

namespace Museums
{
    public static class WatchDisplayUtility
    {
        public static bool TryFindBestWatchCell(Building display, Pawn pawn, bool desireSit, out IntVec3 result, out Building chair)
        {
            chair = null;
            result = default;
            ThingComp_WatchableMuseumBuilding watchableComp = display.GetComp<ThingComp_WatchableMuseumBuilding>();
            if (watchableComp == null)
            {
                return false;
            }
            IEnumerable<IntVec3> potentialCells = watchableComp.AllCurrentlyValidWatchCellsFor(pawn).InRandomOrder();

            if (desireSit)
            {
                IEnumerable<IntVec3> sittablePotentialCells = potentialCells.Where(cell => CanSitIn(cell, pawn));
                // only if there are any sittable cells available should we try using them
                if (!sittablePotentialCells.EnumerableNullOrEmpty())
                {
                    potentialCells = sittablePotentialCells;
                }
            }
            result = potentialCells.RandomElementWithFallback();
            if(result == default)
            {
                return false;
            }
            chair = result.GetEdifice(pawn.Map);
            return true;
        }

        static MethodInfo everPossibleToWatchFromMI = AccessTools.Method(typeof(WatchBuildingUtility), "EverPossibleToWatchFrom");
        public static bool EverPossibleToWatchFrom(IntVec3 watchCell, IntVec3 buildingCenter, Map map, bool bedAllowed, ThingDef def)
        {
            return (bool)everPossibleToWatchFromMI.Invoke(null, new object[] { watchCell, buildingCenter, map, bedAllowed, def });
        }
        public static bool EverPossibleToWatchFrom(IntVec3 watchCell, Thing thing, bool bedAllowed)
        {
            // base game doesn't check this. No idea why, you can't really look at something all that well when standing on top of it.
            if (thing.OccupiedRect().Contains(watchCell))
            {
                return false;
            }
            return EverPossibleToWatchFrom(watchCell, thing.Position, thing.Map, bedAllowed, thing.def);
        }

        private static bool CanSitIn(IntVec3 cell, Pawn pawn)
        {
            Building seat = cell.GetEdifice(pawn.Map);
            if(seat == null)
            {
                return false;
            }
            if (!seat.def.building.isSittable)
            {
                return false;
            }
            if (!pawn.CanReserve(seat))
            {
                return false;
            }
            return true;
        }

        public static Rot4 RotationToLookAt(IntVec3 origin, IntVec3 target)
        {
            return Rot4.FromAngleFlat((target - origin).AngleFlat);
        }

        public static Rot4 RotationToLookAt(Vector3 origin, Vector3 target)
        {
            return Rot4.FromAngleFlat((target - origin).AngleFlat());
        }
    }
}
