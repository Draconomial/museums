﻿using HarmonyLib;
using Mono.Unix.Native;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Museums
{
    public abstract class WatchableCellProvider : IExposable
    {
        protected Thing thing;

        private static List<Type> _existingProviderTypes;
        public static List<Type> ExistingProviderTypes
        {
            get
            {
                if(_existingProviderTypes == null)
                {
                    _existingProviderTypes = typeof(WatchableCellProvider).AllSubclassesNonAbstract();
                }
                return _existingProviderTypes;
            }
        }
        protected List<IntVec3> cells = new List<IntVec3>();
        public List<IntVec3> Cells => cells;

        public WatchableCellProvider() { }

        public WatchableCellProvider(Thing thing)
        {
            this.thing = thing;
            cells = AutoProvideCells().ToList();
        }

        public static WatchableCellProvider MakeProvider(Type type, Thing thing)
        {
            WatchableCellProvider provider = (WatchableCellProvider)Activator.CreateInstance(type, new object[] { thing });
            return provider;
        }

        public static string LabelFor(Type type)
        {
            return $"Museums_{type.Name}".Translate();
        }

        public void ResetForNewPosition()
        {
            cells.Clear();
            cells = AutoProvideCells().ToList();
        }

        protected virtual IEnumerable<IntVec3> AutoProvideCells()
        {
            yield break;
        }

        public virtual IEnumerable<Gizmo> AdditionalGizmos()
        {
            yield return new Command_Action()
            {
                defaultLabel = "Museums_DesignateAddWatchCell".Translate(),
                defaultDesc = "Museums_DesignateAddWatchCell_Desc".Translate(),
                icon = ContentFinder<Texture2D>.Get("Museums/UI/AddWatch", true),
                action = AddWatchCells
            };
            yield return new Command_Action()
            {
                defaultLabel = "Museums_DesignateRemoveWatchCell".Translate(),
                defaultDesc = "Museums_DesignateRemoveWatchCell_Desc".Translate(),
                icon = ContentFinder<Texture2D>.Get("Museums/UI/RemoveWatch", true),
                action = RemoveWatchCells
            };
        }

        private void AddWatchCells()
        {
            Designator_AddDisplayWatchCell designator = new Designator_AddDisplayWatchCell(this, thing);
            Find.DesignatorManager.Select(designator);
        }

        private void RemoveWatchCells()
        {
            Designator_RemoveDisplayWatchCell designator = new Designator_RemoveDisplayWatchCell(this, thing);
            Find.DesignatorManager.Select(designator);
        }

        public void RemoveWatchCellFromDesignator(IntVec3 cell)
        {
            cells.Remove(cell);
        }

        public virtual void ExposeData()
        {
            Scribe_Collections.Look(ref cells, nameof(cells), LookMode.Value);
            Scribe_References.Look(ref thing, nameof(thing));
        }
    }

    public class WatchableCellProvider_Adjacent : WatchableCellProvider
    {
        public WatchableCellProvider_Adjacent() { }

        public WatchableCellProvider_Adjacent(Thing thing) : base(thing) { }

        protected override IEnumerable<IntVec3> AutoProvideCells()
        {
            return thing.OccupiedRect().ExpandedBy(1);
        }
    }

    public class WatchableCellProvider_NearestStandable : WatchableCellProvider
    {
        List<IntVec3> edges = new List<IntVec3>();
        const float maxDistance = 4f;
        const float maxDistanceSquared = maxDistance * maxDistance;

        public WatchableCellProvider_NearestStandable() { }

        public WatchableCellProvider_NearestStandable(Thing thing) : base(thing) { }

        protected override IEnumerable<IntVec3> AutoProvideCells()
        {
            edges.Clear();
            FloodFiller floodFiller = new FloodFiller(thing.Map);
            IEnumerable<IntVec3> extraRoots = thing.OccupiedRect()
                .Where(pos => pos != thing.Position);
            floodFiller.FloodFill(thing.Position, CellValidator, (IntVec3 p) => { }, extraRoots: extraRoots);
            return edges;
        }

        private bool CellValidator(IntVec3 position)
        {
            if(position == thing.Position)
            {
                return true;
            }

            if(edges.Contains(position))
            {
                return false;
            }

            if (thing.OccupiedRect().Contains(position))
            {
                return false;
            }

            // checks if the position is a wall
            if (!position.CanBeSeenOverFast(thing.Map))
            {
                return false;
            }

            if (!GenSight.LineOfSight(position, thing.Position, thing.Map))
            {
                return false;
            }

            if(position.DistanceToSquared(thing.Position) > maxDistanceSquared)
            {
                return false;
            }

            if (position.Standable(thing.Map))
            {
                edges.Add(position);
                return false;
            }
            return true;
        }
    }

    public class WatchableCellProvider_UserDetermined : WatchableCellProvider
    {
        public WatchableCellProvider_UserDetermined() { }

        public WatchableCellProvider_UserDetermined(Thing thing) : base(thing) { }
    }
}
