﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Museums
{
    public class ThingCompProperties_SelectableExtraGraphic : CompProperties
    {
        public bool optional = false;
        public string gizmoLabel;
        public string resetLabel;
        public AltitudeLayer altitudeLayer;

        public List<SelectableExtraGraphicEntry> graphicOptions = new List<SelectableExtraGraphicEntry>();

        public override IEnumerable<string> ConfigErrors(ThingDef parentDef)
        {
            foreach (string error in base.ConfigErrors(parentDef))
            {
                yield return error;
            }

            if(optional && resetLabel == null)
                yield return $"{nameof(optional)} is set, but no {nameof(resetLabel)} is provided!";

            if (graphicOptions.NullOrEmpty())
            {
                yield return $"List \"{nameof(graphicOptions)}\" is null or empty";
            }
            else
            {
                List<string> knownLabels = new List<string>();
                for (int i = 0; i < graphicOptions.Count; i++)
                {
                    SelectableExtraGraphicEntry option = graphicOptions[i];
                    if (!knownLabels.Contains(option.label))
                    {
                        knownLabels.Add(option.label);
                    }
                    else
                    {
                        yield return $"Options are not unique. Label {option.label} is used multiple times. Will cause scribing issues.";
                    }
                    foreach (string error in option.ConfigErrors())
                    {
                        yield return error;
                    }
                }
            }
        }

        public IEnumerable<FloatMenuOption> FloatMenuOptionsFor(ThingComp_SelectableExtraGraphic parentComp)
        {
            return graphicOptions.Select(go => go.FloatMenuOptionFor(parentComp, gizmoLabel));
        }
    }

    public class SelectableExtraGraphicEntry
    {
        public string label;
        string iconTexturePath;
        public GraphicData graphicData;

        bool isTextureCached = false;
        Texture2D _texture;
        Texture2D IconTexture
        {
            get
            {
                if(!isTextureCached)
                {
                    if(iconTexturePath != null)
                    {
                        _texture = ContentFinder<Texture2D>.Get(iconTexturePath, false);
                    }
                    isTextureCached = true;
                }
                return _texture;
            }
        }

        public FloatMenuOption FloatMenuOptionFor(ThingComp_SelectableExtraGraphic parentComp, string baseLabel)
        {
            Action optionAction = () =>
            {
                parentComp.activeSelectableExtraGraphic = this;
            };
            if(IconTexture == null)
            {
                return new FloatMenuOption(label, optionAction);
            }
            else
            {
                return new FloatMenuOption(label, optionAction, IconTexture, Color.white);
            }
        }

        public IEnumerable<string> ConfigErrors()
        {
            if (label == null)
                yield return $"Required field \"{nameof(label)}\" is not set";
            if (graphicData == null)
                yield return $"Required field \"{nameof(graphicData)}\" is not set";
            
        }
    }

    [StaticConstructorOnStartup]
    public class ThingComp_SelectableExtraGraphic : ThingComp
    {
        ThingCompProperties_SelectableExtraGraphic Props => props as ThingCompProperties_SelectableExtraGraphic;
        public SelectableExtraGraphicEntry activeSelectableExtraGraphic;
        string activeSelectableGraphicLabel = null;

        static Texture2D changeStyleTexture = ContentFinder<Texture2D>.Get("Museums/UI/ChangeStyle");
        public override IEnumerable<Gizmo> CompGetGizmosExtra()
        {
            foreach (Gizmo gizmo in base.CompGetGizmosExtra())
            {
                yield return gizmo;
            }
            yield return new Command_Action()
            {
                defaultLabel = Props.gizmoLabel,
                icon = changeStyleTexture,
                action = OpenFloatMenu,
            };
        }

        private void OpenFloatMenu()
        {
            List<FloatMenuOption> options = new List<FloatMenuOption>();
            if (activeSelectableExtraGraphic != null && Props.optional)
            {
                Action resetAction = () => { activeSelectableExtraGraphic = null; };
                options.Add(new FloatMenuOption(Props.resetLabel, resetAction));
            }

            options.AddRange(Props.FloatMenuOptionsFor(this));
            FloatMenu floatMenu = new FloatMenu(options);
            Find.WindowStack.Add(floatMenu);
        }

        public override void PostDraw()
        {
            base.PostDraw();
            if(activeSelectableExtraGraphic == null)
            {
                return;
            }

            Vector3 drawPos = parent.DrawPos;
            drawPos.y += Props.altitudeLayer.AltitudeFor();
            activeSelectableExtraGraphic.graphicData.Graphic.Draw(drawPos, parent.Rotation, parent);
        }

        public override void PostExposeData()
        {
            base.PostExposeData();
            if(Scribe.mode == LoadSaveMode.Saving)
            {
                activeSelectableGraphicLabel = activeSelectableExtraGraphic.label;
            }
            Scribe_Values.Look(ref activeSelectableGraphicLabel, nameof(activeSelectableGraphicLabel));
            if(Scribe.mode == LoadSaveMode.PostLoadInit && activeSelectableGraphicLabel != null)
            {
                activeSelectableExtraGraphic = Props.graphicOptions.FirstOrDefault(entry => entry.label == activeSelectableGraphicLabel);
            }
        }
    }
}
