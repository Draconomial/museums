﻿using HarmonyLib;
using NightmareCore.Utilities;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using Verse;
using Verse.AI;
using static HarmonyLib.Code;

namespace Museums
{
    public class ThingCompProperties_WatchableMuseumBuilding : CompProperties
    {
        public List<GraphicData> additionalGraphicsWhenEmpty = new List<GraphicData>();
        public List<GraphicData> additionalGraphicsWhenFilled = new List<GraphicData>();
        public Vector3 displayedItemOffset = Vector3.zero;
        public float watchRadiusRange = 1.5f;
        public bool onlyWatchableIfContainingItem = false;


        public override IEnumerable<string> ConfigErrors(ThingDef parentDef)
        {
            foreach (string error in base.ConfigErrors(parentDef))
                yield return error;
            if(!IsContainingItemLogicValid(parentDef))
            {
                yield return $"has {nameof(ThingCompProperties_WatchableMuseumBuilding)} with {nameof(onlyWatchableIfContainingItem)}=true, but is not considered a container by this mod)";
            }
        }

        private bool IsContainingItemLogicValid(ThingDef parentDef)
        {
            if (!onlyWatchableIfContainingItem)
            {
                return true;
            }
            if (ContainerUtility.IsContainer(parentDef))
            {
                return true;
            }
            return false;
        }
    }

    [StaticConstructorOnStartup]
    public class ThingComp_WatchableMuseumBuilding : ThingComp
    {
        ThingCompProperties_WatchableMuseumBuilding Props => base.props as ThingCompProperties_WatchableMuseumBuilding;

        bool isAllowedToBeWatched = true;

        WatchableCellProvider _cellProvider;
        WatchableCellProvider CellProvider
        {
            get
            {
                if(_cellProvider == null)
                {
                    _cellProvider = new WatchableCellProvider_NearestStandable(parent);
                }
                return _cellProvider;
            }
        }

        public List<IntVec3> AllWatchCells => CellProvider.Cells;
        public IEnumerable<IntVec3> AllValidWatchCells => AllWatchCells.Where(c => WatchDisplayUtility.EverPossibleToWatchFrom(c, parent, false));
        public IEnumerable<IntVec3> AllCurrentlyValidWatchCellsFor(Pawn p) => AllValidWatchCells.Where(c => Verse.AI.ReservationUtility.CanReserve(p, new LocalTargetInfo(c)));

        public AcceptanceReport IsWatchable
        {
            get
            {
                if (!isAllowedToBeWatched)
                {
                    return "UserDisabledViaGizmo";
                }
                if (Props.onlyWatchableIfContainingItem && !ContainerUtility.ContainsItem(parent))
                {
                    return "DoesNotContainItem";
                }
                if (AllValidWatchCells.EnumerableNullOrEmpty())
                {
                    return "NoValidWatchCells";
                }
                return true;
            }
        }

        public override void PostDrawExtraSelectionOverlays()
        {
            base.PostDrawExtraSelectionOverlays();
            if (isAllowedToBeWatched)
            {
                GenDraw.DrawFieldEdges(AllWatchCells);
                foreach (IntVec3 watchCell in AllValidWatchCells)
                {
                    GenDraw.DrawLineBetween(watchCell.ToVector3Shifted(), parent.TrueCenter());
                }
            }
        }

        public override void PostSpawnSetup(bool respawningAfterLoad)
        {
            base.PostSpawnSetup(respawningAfterLoad);
            if (!respawningAfterLoad)
            {
                CellProvider.ResetForNewPosition();
            }
        }

        public override IEnumerable<Gizmo> CompGetGizmosExtra()
        {
            foreach (Gizmo gizmo in base.CompGetGizmosExtra())
            {
                yield return gizmo;
            }

            yield return ToggleWatchableGizmo;
            if (isAllowedToBeWatched)
            {
                yield return ChangeProviderGizmo;

                if (CellProvider != null)
                {
                    foreach (Gizmo gizmo in CellProvider.AdditionalGizmos())
                    {
                        yield return gizmo;
                    }
                }
            }
        }

        static Texture2D changeProviderTypeIcon = ContentFinder<Texture2D>.Get("Museums/UI/ChangeWatchProvider");
        private Gizmo ChangeProviderGizmo
        {
            get
            {
                return new Command_Action()
                {
                    action = OpenChangeProviderTypeFloatMenu,
                    icon = changeProviderTypeIcon,
                    defaultLabel = "Museums_ChangeWatchProvider".Translate(),
                    defaultDesc = "Museums_ChangeWatchProvider_Desc".Translate(),
                };
            }
        }
        static Texture2D toggleWatchableGizmoIcon = ContentFinder<Texture2D>.Get("Museums/UI/Watchable");
        private Gizmo ToggleWatchableGizmo
        {
            get
            {
                return new Command_Toggle()
                {
                    isActive = () => isAllowedToBeWatched,
                    toggleAction = () => isAllowedToBeWatched = !isAllowedToBeWatched,
                    defaultLabel = "Museums_ToggleWatchable".Translate(),
                    defaultDesc = "Museums_ToggleWatchable_Desc".Translate(),
                    icon = toggleWatchableGizmoIcon
                };
            }
        }

        private void OpenChangeProviderTypeFloatMenu()
        {
            List<FloatMenuOption> options = WatchableCellProvider.ExistingProviderTypes
                .Select(type => new FloatMenuOption(WatchableCellProvider.LabelFor(type), () => ChangeProviderTypeTo(type)))
                .ToList();
            FloatMenu floatMenu = new FloatMenu(options);
            Find.WindowStack.Add(floatMenu);
        }

        private void ChangeProviderTypeTo(Type type)
        {
            _cellProvider = WatchableCellProvider.MakeProvider(type, parent);
        }

        public override void PostExposeData()
        {
            base.PostExposeData();
            Scribe_Deep.Look(ref _cellProvider, nameof(_cellProvider), new object[0]);
            Scribe_Values.Look(ref isAllowedToBeWatched, nameof(isAllowedToBeWatched), true);
        }
    }
}
