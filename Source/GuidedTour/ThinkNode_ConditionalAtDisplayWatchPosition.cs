﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace Museums
{
    public class ThinkNode_ConditionalAtDisplayWatchPosition : ThinkNode_Conditional
    {
        protected override bool Satisfied(Pawn pawn)
        {
            LordJob_Joinable_GuidedMuseumTour lordJob = pawn.GetLord().LordJob as LordJob_Joinable_GuidedMuseumTour;
            return lordJob.HasTourStarted();
        }
    }
}
