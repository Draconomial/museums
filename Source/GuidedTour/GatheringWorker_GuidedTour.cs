﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI.Group;
using Verse.Noise;

namespace Museums
{
    public class GatheringWorker_GuidedTour : GatheringWorker
    {
        public override bool CanExecute(Map map, Pawn organizer = null)
        {
            if(!base.CanExecute(map, organizer))
            {
                return false;
            }
            MapComponent_MuseumTracker museumTracker = map.GetComponent<MapComponent_MuseumTracker>();
            Room randomMuseum = museumTracker.Museums.RandomElementWithFallback();
            if(randomMuseum == null)
            {
                return false;
            }
            return true;
        }

        protected override LordJob CreateLordJob(IntVec3 spot, Pawn organizer)
        {
            return new LordJob_Joinable_GuidedMuseumTour(organizer, spot, base.def);
        }

        protected override bool TryFindGatherSpot(Pawn organizer, out IntVec3 spot)
        {
            spot = default;
            MapComponent_MuseumTracker museumTracker = organizer.Map.GetComponent<MapComponent_MuseumTracker>();
            Room randomMuseum = museumTracker.Museums.RandomElementWithFallback();
            if(randomMuseum == null)
            {
                return false;
            }
            Thing gatheringBuilding = museumTracker.GetMuseumData(randomMuseum)?.GatheringSpot;
            if(gatheringBuilding == null)
            {
                return false;
            }
            spot = gatheringBuilding.Position;
            return true;
            // tours usually start at an entrance (this will not find *the* entrance, but any entrance will do)
            //Thing randomDoor = randomMuseum.ContainedAndAdjacentThings
            //    .Where(t => t.def.IsDoor)
            //    .RandomElementWithFallback();
            //if(randomDoor == null)
            //{
            //    // room has no doors... Odd, don't do anything weird, just use the gathering spot
            //    spot = randomMuseum.
            //    return false;
            //}
            //spot = randomDoor.CellsAdjacent8WayAndInside()
            //    .Where(c => randomMuseum.Cells.Contains(c))
            //    .RandomElementWithFallback();
            //return true;
        }
    }
}
