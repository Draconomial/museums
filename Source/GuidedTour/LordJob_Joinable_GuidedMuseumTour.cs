﻿using HarmonyLib;
using Museums.GuidedTour;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.AI.Group;

namespace Museums
{
    public class LordJob_Joinable_GuidedMuseumTour : LordJob_Joinable_Party
    {
        TourData tourData;
        Dictionary<Pawn, Lord> pawnsWithPreviousLord = new Dictionary<Pawn, Lord>();
        public TourData TourData => tourData;

        protected override ThoughtDef AttendeeThought => Museums_ThoughtDefOf.Museums_AttendedGuidedMuseumTour;
        protected override TaleDef AttendeeTale => Museums_TaleDefOf.Museums_AttendedGuidedMuseumTour;
        protected override ThoughtDef OrganizerThought => Museums_ThoughtDefOf.Museums_OrganizedGuidedMuseumTour;
        protected override TaleDef OrganizerTale => Museums_TaleDefOf.Museums_OrganizedGuidedMuseumTour;

        public LordJob_Joinable_GuidedMuseumTour() { }
        public LordJob_Joinable_GuidedMuseumTour(Pawn organizer, IntVec3 spot, GatheringDef gatheringDef) : base(spot, organizer, gatheringDef)
        {
            tourData = new TourData(organizer, spot);
        }

        public override float VoluntaryJoinPriorityFor(Pawn p)
        {
            if(p.Faction != Faction.OfPlayer)
            {
                return 5f;
            }
            return base.VoluntaryJoinPriorityFor(p);
        }

        /// <summary>
        /// When this is not forced to TRUE visitors that participate in the tour are considered in restraints. I can't even. What a waste of time tracking it down.
        /// </summary>
        public override bool NeverInRestraints => true;

        protected override LordToil CreateGatheringToil(IntVec3 spot, Pawn organizer, GatheringDef gatheringDef)
        {
            return new LordToil_Party(spot, gatheringDef);
        }

        protected override Trigger_TicksPassed GetTimeoutTrigger()
        {
            return new Trigger_TicksPassed(tourData.TourDurationTicks * 4);
        }

        public override string GetReport(Pawn pawn)
        {
            return "".Translate(pawn);
        }

        /// <summary>
        /// When "stealing" pawns from lords of other factions, we must re-assign those pawns to their old lords
        /// </summary>
        public void RememberReassignedPawn(Pawn pawn)
        {
            Lord oldLord = pawn.GetLord();
            if(oldLord == null)
            {
                return;
            }
            if(oldLord.faction == Faction.OfPlayer)
            {
                return;
            }
            pawnsWithPreviousLord.Add(pawn, oldLord);
        }

        public override void Notify_PawnLost(Pawn p, PawnLostCondition condition)
        {
            base.Notify_PawnLost(p, condition);
            TryReassignPawnToPreviousLord(p);
        }

        public override void Cleanup()
        {
            base.Cleanup();
            foreach(Pawn pawn in pawnsWithPreviousLord.Keys.ToList())
            {
                TryReassignPawnToPreviousLord(pawn);
            }
            if(pawnsWithPreviousLord.Any())
            {
                Log.Error($"Cleaning up after lord job, but found {pawnsWithPreviousLord.Count} pawns that were not assigned to their old lord: {pawnsWithPreviousLord.Keys.ToStringSafeEnumerable()}");
            }
        }

        private bool TryReassignPawnToPreviousLord(Pawn pawn)
        {
            //Log.Message($"Trying to reassigne pawn {pawn.LabelShort} with duty {pawn.mindState?.duty?.def?.defName ?? "NONE"}");
            if(!pawnsWithPreviousLord.ContainsKey(pawn))
            {
                return false;
            }
            if (lord.ownedPawns.Contains(pawn))
            {
                // must remove pawn from this lord. Cleanup does *not* remove its tracked pawns, which means that after the lord was re-assigned and handed out its duties the cleanup will loop over this pawn and set its duty to NULL
                lord.RemovePawn(pawn);
            }
            Lord oldLord = pawnsWithPreviousLord[pawn];
            oldLord.AddPawn(pawn);
            //Log.Message($"Assigned pawn {pawn.LabelShort} to their old lord {oldLord.loadID} {oldLord.CurLordToil} - they now have duty {pawn.mindState?.duty?.def?.defName ?? "NONE"}");
            pawnsWithPreviousLord.Remove(pawn);
            return true;
        }

        /// <summary>
        /// By injecting a new trigger to the default transition to end we can "interrupt" the lord job once the tour finishes with all the good stuff of a properly ended lord job
        /// </summary>
        /// <returns></returns>
        public override StateGraph CreateGraph()
        {
            StateGraph graph = base.CreateGraph();

            LordToil_Party partyToil = graph.lordToils.First(toil => toil is LordToil_Party) as LordToil_Party;
            LordToil_End endToil = graph.lordToils.First(toil => toil is LordToil_End) as LordToil_End;

            Transition successTransition = graph.transitions.First(transition => transition.triggers.Any(trigger => trigger is Trigger_TicksPassed));
            successTransition.AddTrigger(new Trigger_GuidedTourFinished());
            successTransition.AddPreAction(new TransitionAction_Custom(ApplyTourOutcome));

            return graph;
        }

        const float chanceForFollowUpSoloTour = 0.5f;
        private void ApplyTourOutcome()
        {
            foreach (Pawn follower in tourData.TourParticipants)
            {
                if (Rand.Chance(chanceForFollowUpSoloTour))
                {
                    Job job = JobMaker.MakeJob(Common.SoloMuseumTourJob, tourData.museumCell);
                    QueuedJob qj = new QueuedJob(job, JobTag.UnspecifiedLordDuty);
                    follower.jobs.jobQueue.AddItem(qj);
                }
            }
        }

        public override void LordJobTick()
        {
            base.LordJobTick();
            tourData.Tick();
        }

        const float percentageInPositionRequired = 0.8f;
        public bool HasTourStarted()
        {
            if (tourData.HasTourStarted)
            {
                return true;
            }
            if (!GatheringsUtility.InGatheringArea(organizer.Position, spot, Map))
            {
                return false;
            }
            int pawnsInPositionCount = lord.ownedPawns.Count(p => GatheringsUtility.InGatheringArea(p.Position, spot, Map));
            bool enoughPawnsInPosition = lord.ownedPawns.Count * percentageInPositionRequired < pawnsInPositionCount;
            //Log.Message($"Pawns in position: {pawnsInPositionCount} out of {lord.ownedPawns.Count}");
            if (enoughPawnsInPosition)
            {
                tourData.StartTour();
            }
            return enoughPawnsInPosition;
        }

        public override bool LostImportantReferenceDuringLoading
        {
            get
            {
                if (base.LostImportantReferenceDuringLoading)
                {
                    return true;
                }
                if(tourData == null)
                {
                    return false;
                }
                AcceptanceReport report = tourData.IsValid;
                if (!report)
                {
                    Log.Error($"Lost important reference during loading: {report.Reason}");
                }
                return report;
            }
        }

        public override void ExposeData()
        {
            base.ExposeData();
            Scribe_Deep.Look(ref tourData, nameof(tourData), new object[0]);
            Scribe_Collections.Look(ref pawnsWithPreviousLord, nameof(pawnsWithPreviousLord), LookMode.Reference, LookMode.Reference);
        }
    }
}
