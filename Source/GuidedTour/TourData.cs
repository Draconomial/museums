﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.AI.Group;
using Verse.Noise;

namespace Museums
{
    public class TourData : IExposable
    {
        int currentBuildingIndex = 0;
        Pawn guide;
        public IntVec3 museumCell;
        Room _museumRoom;
        public Room MuseumRoom
        {
            get
            {
                if(_museumRoom == null)
                {
                    _museumRoom = museumCell.GetRoom(guide.Map);
                }
                return _museumRoom;
            }
        }

        int tourDurationTicks;
        public int TourDurationTicks => tourDurationTicks;
        int tourStartTick = 0;
        public bool HasTourStarted => tourStartTick > 0;
        int tourEndTick = 0;
        public bool HasTourEnded => tourEndTick > 0;
        int actualDurationTicksPerWatchable;
        List<Building> tourFocusBuildings;
        public Building CurrentTourFocusBuilding => tourFocusBuildings[currentBuildingIndex];
        IntVec3 currentGuidePosition;
        public IntVec3 CurrentGuidePosition => currentGuidePosition;
        private bool GuideIsInPosition => guide.Position == CurrentGuidePosition;
        int currentWatchTicks = 0;

        IntRange tourDurationTicksRange = new IntRange(5000, 15000);
        const int maxDisplayCount = 10;

        public AcceptanceReport IsValid
        {
            get
            {
                if(guide == null)
                {
                    return "Scribed guide is NULL";
                }
                if (!museumCell.IsValid)
                {
                    return "Scribed cell in museum is invalid";
                }
                if (tourFocusBuildings.NullOrEmpty())
                {
                    return "No display buildings are set";
                }
                return true;
            }
        }

        public IEnumerable<Pawn> TourParticipants => guide.GetLord().ownedPawns;

        public TourData(Pawn organizer, IntVec3 spot)
        {
            guide = organizer;

            museumCell = spot;
            CalculateDisplayBuildingsForTour();
            tourDurationTicks = tourDurationTicksRange.RandomInRange;
            actualDurationTicksPerWatchable = tourDurationTicks / tourFocusBuildings.Count;
            //Log.Message($"Calculated tour parameters, displays: {tourFocusBuildings.Select(b => b.Position).ToStringSafeEnumerable()}, displays: {tourFocusBuildings.Count}, duration per: {actualDurationTicksPerWatchable}, total: {tourDurationTicks}");
        }
        private void CalculateDisplayBuildingsForTour()
        {
            Map map = guide.Map;
            MuseumData museumData = map.GetComponent<MapComponent_MuseumTracker>()?.GetMuseumData(MuseumRoom);
            tourFocusBuildings = museumData.WatchableBuildings
                .InRandomOrder()
                .Take(maxDisplayCount)
                .ToList();
        }

        public void Tick()
        {
            //if (GenTicks.TicksGame % GenTicks.TickRareInterval == 0)
            //{
            //    Log.Message($"Current index: {currentBuildingIndex}, guide in position ? {GuideIsInPosition}, current display elapsed? {currentWatchTicks > actualDurationTicksPerWatchable}");
            //}
            if (GuideIsInPosition)
            {
                currentWatchTicks++;
            }
            if(currentWatchTicks > actualDurationTicksPerWatchable)
            {
                SetNextDisplay();
            }
        }

        public void StartTour()
        {
            //Log.Message($"Started tour");
            tourStartTick = GenTicks.TicksGame;
            SetGuideParametersForCurrentDisplay();
        }

        private void EndTour()
        {
            //Log.Message($"Ended tour");
            tourEndTick = GenTicks.TicksGame;
        }

        private void SetGuideParametersForCurrentDisplay()
        {
            currentGuidePosition = CurrentTourFocusBuilding.GetComp<ThingComp_WatchableMuseumBuilding>().AllCurrentlyValidWatchCellsFor(guide).RandomElementWithFallback();

            if (guide.CurJobDef == JobDefOf.GiveSpeech)
            {
                guide.jobs.EndCurrentJob(Verse.AI.JobCondition.Succeeded);
                //Log.Message($"Ended guide speech job");
            }
            //else
            //{
            //    Log.Message($"Did not end guide job, is currently: {guide.CurJob}");
            //}
        }

        private void SetNextDisplay()
        {
            //Log.Message($"Set next display");
            int newIndex = currentBuildingIndex + 1;
            if(newIndex >= tourFocusBuildings.Count)
            {
                EndTour();
                return;
            }
            currentBuildingIndex = newIndex;
            currentWatchTicks = 0;

            SetGuideParametersForCurrentDisplay();

            foreach (Pawn follower in TourParticipants)
            {
                // setting follower focus to invalid forced "ConditionalAtDutyLocation" to invalidate, which means the pawn then tries to go to the new "current" display
                follower.mindState.duty.focus = IntVec3.Invalid;
            }
        }

        public void FlashDebugData()
        {
            guide.Map.debugDrawer.FlashCell(CurrentGuidePosition, 0.3f, "Guide", 1);
            
            for (int i = 0; i < tourFocusBuildings.Count; i++)
            {
                var building = tourFocusBuildings[i];
                if(building == CurrentTourFocusBuilding)
                {
                    guide.Map.debugDrawer.FlashCell(building.Position, 0.6f, "Cur Disp", 1);
                }
                else
                {
                    guide.Map.debugDrawer.FlashCell(building.Position, 0.6f, $"Disp {i}", 1);
                }
            }
        }


        public void ExposeData()
        {
            Scribe_Collections.Look(ref tourFocusBuildings, nameof(tourFocusBuildings), LookMode.Reference);
            Scribe_Values.Look(ref currentBuildingIndex, nameof(currentBuildingIndex));
            Scribe_References.Look(ref guide, nameof(guide));
            Scribe_Values.Look(ref tourDurationTicks, nameof(tourDurationTicks));
            Scribe_Values.Look(ref tourStartTick, nameof(tourStartTick));
            Scribe_Values.Look(ref tourEndTick, nameof(tourEndTick));
            Scribe_Values.Look(ref actualDurationTicksPerWatchable, nameof(actualDurationTicksPerWatchable));
            Scribe_Values.Look(ref currentGuidePosition, nameof(currentGuidePosition));
            Scribe_Values.Look(ref currentWatchTicks, nameof(currentWatchTicks));
        }
    }
}
