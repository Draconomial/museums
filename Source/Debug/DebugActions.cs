﻿#if v1_5
using LudeonTK;
#endif
using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace Museums.Debug
{
    public static class DebugActions
    {
        [DebugAction("Museums", "Give Solo Tour Job", actionType = DebugActionType.ToolMapForPawns, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        private static void DEBUG_GiveSoloMuseumTourJob(Pawn p)
        {
            Map map = p.Map;
            MapComponent_MuseumTracker comp = map.GetComponent<MapComponent_MuseumTracker>();
            Room museumRoom = comp.Museums.RandomElementWithFallback();
            if (museumRoom == null)
            {
                Log.Warning($"Map has no museum room");
                return;
            }
            MuseumData museumData = comp.GetMuseumData(museumRoom);
            if (museumData == null)
            {
                Log.Warning($"Museum room has no museum data");
                return;
            }
            Thing sign = museumData.GatheringSpot;
            if (sign == null)
            {
                Log.Warning($"Museum has no gathering spot");
                return;
            }
            Job job = JobMaker.MakeJob(Common.SoloMuseumTourJob, sign);
            Log.Message($"Gave job: {job}");
            p.jobs.TryTakeOrderedJob(job);
        }

        [DebugAction("Museums", "CanWatch", actionType = DebugActionType.ToolMap, allowedGameStates = AllowedGameStates.PlayingOnMap)]
        public static void Debug_CanWatch()
        {
            IntVec3 pos = UI.MouseCell();
            Map map = Find.CurrentMap;
            Thing building = pos.GetThingList(map)?.FirstOrDefault(t => t.def.HasAssignableCompFrom(typeof(ThingComp_WatchableMuseumBuilding)));
            if(building == null)
            {
                Log.Message($"No watchable comp");
                return;
            }
            ThingComp_WatchableMuseumBuilding comp = building.TryGetComp<ThingComp_WatchableMuseumBuilding>();
            Log.Message($"Can watch {building} ? {(comp.IsWatchable ? "Yes" : comp.IsWatchable.Reason)}");
        }
    }
}
