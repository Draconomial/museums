﻿#if !v1_4
using LudeonTK;
#endif
using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Verse;
using Verse.AI.Group;

namespace Museums.Debug
{
    //[HarmonyPatch(typeof(BuildingsDamageSectionLayerUtility), "GetDamageRect")]
    //public class Patch_Debug
    //{
    //    [TweakValue("Museums", 0, 3)]
    //    static float damageX;
    //    [TweakValue("Museums", 0, 3)]
    //    static float damageY;
    //    [TweakValue("Museums", 0, 3)]
    //    static float damageWidth;
    //    [TweakValue("Museums", 0, 3)]
    //    static float damageHeight;

    //    [HarmonyPostfix]
    //    public static void InjectDamageData(ref Rect __result, Building b)
    //    {
    //        CellRect cellRect = b.OccupiedRect();
    //        Rect rect = new Rect((float)cellRect.minX, (float)cellRect.minZ, (float)cellRect.Width, (float)cellRect.Height);

    //        var rect2 = new Rect(rect.x + damageX, rect.y + damageY, damageWidth, damageHeight);
    //        __result = rect2;
    //    }
    //}

    [HarmonyPatch]
    public static class MuseumsDebugPatches
    {
        [TweakValue("Museums", 0, 1)]
        public static bool DEBUGdrawTourRelatedCells = false;

        [HarmonyPatch(typeof(TickManager), nameof(TickManager.DoSingleTick))]
        [HarmonyPostfix]
        public static void DrawTourRelatedCells()
        {
            if (!DEBUGdrawTourRelatedCells)
            {
                return;
            }
            try
            {
                Map map = Find.CurrentMap;
                TourData tour = null;
                foreach (Pawn pawn in map.mapPawns.FreeColonists)
                {
                    if (pawn.GetLord()?.LordJob is LordJob_Joinable_GuidedMuseumTour job)
                    {
                        tour = job.TourData;
                        break;
                    }
                }
                if (tour == null)
                {
                    return;
                }
                tour.FlashDebugData();
            }
            catch(Exception e)
            {
                Log.Error($"Caught unhandled exception: {e}");
            }
        }
    }
}
