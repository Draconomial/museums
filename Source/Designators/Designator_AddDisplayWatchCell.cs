﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace Museums
{
    public class Designator_AddDisplayWatchCell : Designator_Cells
    {
        WatchableCellProvider provider;
        Thing thing;
        public Designator_AddDisplayWatchCell(WatchableCellProvider provider, Thing thing)
        {
            this.provider = provider;
            this.thing = thing;

            base.icon = ContentFinder<Texture2D>.Get("Museums/UI/AddWatch", true);
            base.soundDragSustain = SoundDefOf.Designate_DragStandard;
            base.soundDragChanged = SoundDefOf.Designate_DragStandard_Changed;
            base.useMouseIcon = true;
        }

        public override int DraggableDimensions => 2;

        public override void DesignateSingleCell(IntVec3 c)
        {
            provider.Cells.Add(c);
        }

        public override AcceptanceReport CanDesignateCell(IntVec3 loc)
        {
            return WatchDisplayUtility.EverPossibleToWatchFrom(loc, thing, false);
        }
    }
}
