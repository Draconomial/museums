﻿using HarmonyLib;
using NightmareCore;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI;

namespace Museums
{
    public static class Common
    {
        public static ThingDef Sign = DefDatabase<ThingDef>.GetNamed("Museums_Sign");
        public static ThingDef GatheringSpot = DefDatabase<ThingDef>.GetNamed("Museums_TourGatheringSpot");
        public static RoomRoleDef RoomRole_Museum = DefDatabase<RoomRoleDef>.GetNamed("Museums_Museum");
        public static JobDef SoloMuseumTourJob = DefDatabase<JobDef>.GetNamed("Museums_SoloMuseumTour");
        public static GatheringDef GuidedTourGatheringDef = DefDatabase<GatheringDef>.GetNamed("Museums_GuidedTour");
        public static DutyDef GuidedTourDutyDef = DefDatabase<DutyDef>.GetNamed("Museums_GuidedTour");
    }
}
