﻿using HarmonyLib;
using RimWorld;
using RimWorld.QuestGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;

namespace Museums
{
    public class Building_TourGatherSpot : Building
    {
        MuseumData _museumData;
        MuseumData MuseumData
        {
            get
            {
                if(_museumData == null)
                {
                    _museumData = Map.GetComponent<MapComponent_MuseumTracker>().GetMuseumData(this.GetRoom());
                }
                return _museumData;
            }
        }

        public override string GetInspectString()
        {
            string text = base.GetInspectString();
            if(text != string.Empty)
            {
                text += "\n";
            }
            AcceptanceReport tourableReport = CanBeToured();
            if (tourableReport)
            {
                int watchableBuildingCount = MuseumData.WatchableBuildings.Count();
                text += $"{"Museums_InspectString_WatchableBuildings".Translate(watchableBuildingCount.Named("COUNT"))}";
            }
            else
            {
                text += $"{"Museums_InspectString_CannotBeToured".Translate(tourableReport.Reason.Named("REASON"))}";
            }

            return text;
        }

        private AcceptanceReport CanBeToured()
        {
            Room room = this.GetRoom();
            if (room == null || room.Role != Common.RoomRole_Museum)
            {
                return "Museums_InspectString_InvalidTourReason_RoomNotMuseum".Translate();
            }

            return MuseumData.CanBeToured;
        }

        /// <summary>
        /// Hook for integrations to add more gizmos for tour-ability
        /// </summary>
        public override IEnumerable<Gizmo> GetGizmos()
        {
            return base.GetGizmos();
        }
    }
}
