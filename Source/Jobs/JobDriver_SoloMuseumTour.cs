﻿using HarmonyLib;
using RimWorld;
using RimWorld.BaseGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine.UIElements;
using Verse;
using Verse.AI;

namespace Museums
{
    public class JobDriver_SoloMuseumTour : JobDriver
    {
        const int watchDurationTicks = 500;
        const int totalTourTicks = 4000;

        TargetIndex museumMarkerIndex = TargetIndex.A;
        TargetIndex displayStandPositionIndex = TargetIndex.B;
        TargetIndex displayIndex = TargetIndex.C;

        public Thing MuseumMarker => base.job.GetTarget(museumMarkerIndex).Thing;
        public IntVec3 DisplayStandPosition => base.job.GetTarget(displayStandPositionIndex).Cell;
        public Building Display => base.job.GetTarget(displayIndex).Thing as Building;

        public MuseumData MuseumData
        {
            get
            {
                return MuseumMarker.Map.GetComponent<MapComponent_MuseumTracker>().GetMuseumData(MuseumMarker.GetRoom());
            }
        }

        public override bool TryMakePreToilReservations(bool errorOnFailed)
        {
            return true;
        }

        protected override IEnumerable<Toil> MakeNewToils()
        {
            base.AddFailCondition(FailOnMuseumDataInvalid);
            Toil setNextDisplayToil = Toils_General.Do(SetTargetDisplay);
            yield return setNextDisplayToil;

            Toil gotoDisplayToil = Toils_Goto.GotoCell(displayStandPositionIndex, PathEndMode.OnCell);
            yield return gotoDisplayToil;

            Toil watchDisplayToil = Toils_General.Wait(watchDurationTicks, displayIndex);
            yield return watchDisplayToil;

            Toil jumpToil = Toils_Jump.JumpIf(setNextDisplayToil, ShouldWatchOtherDisplay);
            yield return jumpToil;
        }

        private bool FailOnMuseumDataInvalid()
        {
            MuseumData data = MuseumData;
            if(data == null)
            {
                return true;
            }
            if (!data.CanBeToured)
            {
                return true;
            }
            return false;
        }

        private bool ShouldWatchOtherDisplay()
        {
            bool hasTourTimedOut = GenTicks.TicksGame > base.startTick + totalTourTicks;
            return !hasTourTimedOut;
        }

        private void SetTargetDisplay()
        {
            IEnumerable<Building> availableDisplays = MuseumData.WatchableBuildings;
            //Log.Message($"Available target displays: {availableDisplays.ToStringSafeEnumerable()}");
            if (availableDisplays.EnumerableNullOrEmpty())
            {
                return;
            }

            Building currentDisplay = availableDisplays.RandomElement();
            base.job.SetTarget(displayIndex, currentDisplay);

            SetTargetCell();
        }

        private void SetTargetCell()
        {
            IntVec3 standCell;
            if(!TryFindWatchCell(out standCell))
            {
                standCell = Display.CellsAdjacent8WayAndInside()
                    .Where(c => c.Standable(Map))
                    .RandomElementWithFallback();
                //Log.Message($"Used surrounding cells: {standCell}");
            }
            if (standCell == default || !standCell.IsValid)
            {
                //Log.Warning($"No cell found to watch display building {Display}");
                base.EndJobWith(JobCondition.Errored);
                return;
            }
            //Log.Message($"Set target cell: {standCell}");
            base.job.SetTarget(displayStandPositionIndex, standCell);
        }

        private bool TryFindWatchCell(out IntVec3 position)
        {
            // randomly try to sit down or stand to watch
            bool desireSit = Rand.Chance(0.5f);

            if (WatchDisplayUtility.TryFindBestWatchCell(Display, base.pawn, desireSit: desireSit, out position, out _))
            {
                //Log.Message($"Found watch cell with sit ? {desireSit}: {position}");
                return true;
            }
            return false;
        }
    }
}
