﻿using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Verse;
using Verse.AI;
using Verse.Noise;

namespace Museums
{
    public class JoyGiver_SoloMuseumTour : JoyGiver
    {
        public override Job TryGiveJob(Pawn pawn)
        {
            MapComponent_MuseumTracker tracker = pawn.Map.GetComponent<MapComponent_MuseumTracker>();
            IEnumerable<Room> museums = tracker.Museums;
            if (museums.EnumerableNullOrEmpty())
            {
                return null;
            }

            Room museumRoom = museums.RandomElementWithFallback();
            if (museumRoom == null)
            {
                return null;
            }

            MuseumData museumData = tracker.GetMuseumData(museumRoom);
            if (museumData == null)
            {
                return null;
            }

            Thing gatheringSpot = museumData.GatheringSpot;
            if (gatheringSpot == null)
            {
                return null;
            }

            Job job = JobMaker.MakeJob(Common.SoloMuseumTourJob, gatheringSpot);
            return job;
        }
    }
}
